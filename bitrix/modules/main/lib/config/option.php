<?php
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2015 Bitrix
 */
namespace Bitrix\Main\Config;

use Bitrix\Main;

class Option
{
	const CACHE_DIR = "b_option";

	protected static $options = array();

	/**
	 * Returns a value of an option.
	 *
	 * @param string $moduleId The module ID.
	 * @param string $name The option name.
	 * @param string $default The default value to return, if a value doesn't exist.
	 * @param bool|string $siteId The site ID, if the option differs for sites.
	 * @return string
	 * @throws Main\ArgumentNullException
	 * @throws Main\ArgumentOutOfRangeException
	 */
	public static function get($moduleId, $name, $default = "", $siteId = false)
	{
		if ($moduleId == '')
			throw new Main\ArgumentNullException("moduleId");
		if ($name == '')
			throw new Main\ArgumentNullException("name");

		if (!isset(self::$options[$moduleId]))
		{
			static::load($moduleId);
		}

		if ($siteId === false)
		{
			$siteId = static::getDefaultSite();
		}

		$siteKey = ($siteId == ""? "-" : $siteId);

		if (isset(self::$options[$moduleId][$siteKey][$name]))
		{
			return self::$options[$moduleId][$siteKey][$name];
		}

		if (isset(self::$options[$moduleId]["-"][$name]))
		{
			return self::$options[$moduleId]["-"][$name];
		}

		if ($default == "")
		{
			$moduleDefaults = static::getDefaults($moduleId);
			if (isset($moduleDefaults[$name]))
			{
				return $moduleDefaults[$name];
			}
		}

		return $default;
	}

	/**
	 * Returns the real value of an option as it's written in a DB.
	 *
	 * @param string $moduleId The module ID.
	 * @param string $name The option name.
	 * @param bool|string $siteId The site ID.
	 * @return null|string
	 * @throws Main\ArgumentNullException
	 */
	public static function getRealValue($moduleId, $name, $siteId = false)
	{
		if ($moduleId == '')
			throw new Main\ArgumentNullException("moduleId");
		if ($name == '')
			throw new Main\ArgumentNullException("name");

		if (!isset(self::$options[$moduleId]))
		{
			static::load($moduleId);
		}

		if ($siteId === false)
		{
			$siteId = static::getDefaultSite();
		}

		$siteKey = ($siteId == ""? "-" : $siteId);

		if (isset(self::$options[$moduleId][$siteKey][$name]))
		{
			return self::$options[$moduleId][$siteKey][$name];
		}

		return null;
	}

	/**
	 * Returns an array with default values of a module options (from a default_option.php file).
	 *
	 * @param string $moduleId The module ID.
	 * @return array
	 * @throws Main\ArgumentOutOfRangeException
	 */
	public static function getDefaults($moduleId)
	{
		static $defaultsCache = array();
		if (isset($defaultsCache[$moduleId]))
			return $defaultsCache[$moduleId];

		if (preg_match("#[^a-zA-Z0-9._]#", $moduleId))
			throw new Main\ArgumentOutOfRangeException("moduleId");

		$path = Main\Loader::getLocal("modules/".$moduleId."/default_option.php");
		if ($path === false)
			return $defaultsCache[$moduleId] = array();

		include($path);

		$varName = str_replace(".", "_", $moduleId)."_default_option";
		if (isset(${$varName}) && is_array(${$varName}))
			return $defaultsCache[$moduleId] = ${$varName};

		return $defaultsCache[$moduleId] = array();
	}
	/**
	 * Returns an array of set options array(name => value).
	 *
	 * @param string $moduleId The module ID.
	 * @param bool|string $siteId The site ID, if the option differs for sites.
	 * @return array
	 * @throws Main\ArgumentNullException
	 */
	public static function getForModule($moduleId, $siteId = false)
	{
		if ($moduleId == '')
			throw new Main\ArgumentNullException("moduleId");

		if (!isset(self::$options[$moduleId]))
		{
			static::load($moduleId);
		}

		if ($siteId === false)
		{
			$siteId = static::getDefaultSite();
		}

		$result = self::$options[$moduleId]["-"];

		if($siteId <> "" && !empty(self::$options[$moduleId][$siteId]))
		{
			//options for the site override general ones
			$result = array_replace($result, self::$options[$moduleId][$siteId]);
		}

		return $result;
	}

	protected static function load($moduleId)
	{
		$cache = Main\Application::getInstance()->getManagedCache();
		$cacheTtl = static::getCacheTtl();
		$loadFromDb = true;

		if ($cacheTtl !== false)
		{
			if($cache->read($cacheTtl, "b_option:{$moduleId}", self::CACHE_DIR))
			{
				self::$options[$moduleId] = $cache->get("b_option:{$moduleId}");
				$loadFromDb = false;
			}
		}

		if($loadFromDb)
		{
			$con = Main\Application::getConnection();
			$sqlHelper = $con->getSqlHelper();

			self::$options[$moduleId] = ["-" => []];

			$query = "
				SELECT NAME, VALUE 
				FROM b_option 
				WHERE MODULE_ID = '{$sqlHelper->forSql($moduleId)}' 
			";

			$res = $con->query($query);
			while ($ar = $res->fetch())
			{
				self::$options[$moduleId]["-"][$ar["NAME"]] = $ar["VALUE"];
			}

			try
			{
				//b_option_site possibly doesn't exist

				$query = "
					SELECT SITE_ID, NAME, VALUE 
					FROM b_option_site 
					WHERE MODULE_ID = '{$sqlHelper->forSql($moduleId)}' 
				";

				$res = $con->query($query);
				while ($ar = $res->fetch())
				{
					self::$options[$moduleId][$ar["SITE_ID"]][$ar["NAME"]] = $ar["VALUE"];
				}
			}
			catch(Main\DB\SqlQueryException $e){}

			if($cacheTtl !== false)
			{
				$cache->set("b_option:{$moduleId}", self::$options[$moduleId]);
			}
		}

		/*ZDUyZmZZTg3YjI4NmE0ZjE4NDY4ZTNlMGU0ZjIyZTkxODgzODk=*/$GLOBALS['____1678407798']= array(base64_decode('ZXh'.'w'.'b'.'G'.'9kZ'.'Q=='),base64_decode('cGFjaw=='),base64_decode('bWQ'.'1'),base64_decode('Y29uc3'.'RhbnQ='),base64_decode('aGFzaF9o'.'bWFj'),base64_decode(''.'c3RyY21w'),base64_decode('aXNfb2'.'Jq'.'ZWN0'),base64_decode('Y2'.'FsbF9'.'1c2VyX2Z1'.'bmM='),base64_decode('Y2FsbF91c2VyX'.'2Z1'.'b'.'mM='),base64_decode('Y2Fsb'.'F91c2'.'VyX2Z1bm'.'M'.'='),base64_decode(''.'Y2'.'F'.'s'.'bF91c2VyX'.'2Z1'.'b'.'mM='),base64_decode(''.'Y2FsbF9'.'1'.'c2'.'VyX2Z1bmM'.'='));if(!function_exists(__NAMESPACE__.'\\___1539217067')){function ___1539217067($_921836569){static $_1708854729= false; if($_1708854729 == false) $_1708854729=array('LQ==','b'.'WFpb'.'g==','bWFp'.'b'.'g==','LQ'.'==','b'.'WFpbg==','f'.'l'.'BBUkFNX01'.'BWF9VU0VSUw='.'=','LQ==',''.'bWF'.'pb'.'g==','fl'.'BBUkFNX0'.'1B'.'WF9VU'.'0V'.'SUw==','Lg==','SCo=','Yml0'.'cml4','TElDR'.'U5TR'.'V9LRV'.'k=','c'.'2'.'hhMjU2','LQ==','bWFpbg'.'==','flBBU'.'kFNX01BWF9V'.'U0'.'VSUw==',''.'L'.'Q='.'=','bWF'.'pb'.'g==',''.'UEF'.'SQU1'.'fTUFYX1VTR'.'V'.'JT','VVNFU'.'g==','VV'.'NF'.'Ug==',''.'V'.'VNFUg==','S'.'X'.'NBdXRo'.'b'.'3JpemVk','V'.'V'.'NFUg==',''.'SXNBZG1'.'pbg'.'==','QV'.'BQTEl'.'DQVRJT0'.'4=','U'.'mVz'.'dGFy'.'dEJ1ZmZlc'.'g'.'==','TG9jYWxS'.'Z'.'W'.'RpcmVjdA'.'==','L2'.'xpY'.'2Vuc2VfcmVzd'.'HJ'.'p'.'Y3R'.'pb24ucGh'.'w','L'.'Q'.'='.'=',''.'bW'.'Fpbg==','f'.'lBBUkFNX01BWF9'.'VU0V'.'SUw==','LQ='.'=','bWFpbg==','UE'.'FS'.'QU1'.'fTUFYX1VTR'.'VJT','XEJp'.'d'.'H'.'JpeFxNYW'.'lu'.'XENvb'.'mZpZ1xP'.'cH'.'Rpb2'.'46OnNldA==','bW'.'Fpb'.'g='.'=','U'.'EF'.'SQU1fTUFYX'.'1VT'.'RV'.'JT');return base64_decode($_1708854729[$_921836569]);}};if(isset(self::$options[___1539217067(0)][___1539217067(1)]) && $moduleId === ___1539217067(2)){ if(isset(self::$options[___1539217067(3)][___1539217067(4)][___1539217067(5)])){ $_1472114767= self::$options[___1539217067(6)][___1539217067(7)][___1539217067(8)]; list($_865252163, $_518177938)= $GLOBALS['____1678407798'][0](___1539217067(9), $_1472114767); $_496798863= $GLOBALS['____1678407798'][1](___1539217067(10), $_865252163); $_818063333= ___1539217067(11).$GLOBALS['____1678407798'][2]($GLOBALS['____1678407798'][3](___1539217067(12))); $_684129819= $GLOBALS['____1678407798'][4](___1539217067(13), $_518177938, $_818063333, true); self::$options[___1539217067(14)][___1539217067(15)][___1539217067(16)]= $_518177938; self::$options[___1539217067(17)][___1539217067(18)][___1539217067(19)]= $_518177938; if($GLOBALS['____1678407798'][5]($_684129819, $_496798863) !==(922-2*461)){ if(isset($GLOBALS[___1539217067(20)]) && $GLOBALS['____1678407798'][6]($GLOBALS[___1539217067(21)]) && $GLOBALS['____1678407798'][7](array($GLOBALS[___1539217067(22)], ___1539217067(23))) &&!$GLOBALS['____1678407798'][8](array($GLOBALS[___1539217067(24)], ___1539217067(25)))){ $GLOBALS['____1678407798'][9](array($GLOBALS[___1539217067(26)], ___1539217067(27))); $GLOBALS['____1678407798'][10](___1539217067(28), ___1539217067(29), true);} return;}} else{ self::$options[___1539217067(30)][___1539217067(31)][___1539217067(32)]= round(0+3+3+3+3); self::$options[___1539217067(33)][___1539217067(34)][___1539217067(35)]= round(0+3+3+3+3); $GLOBALS['____1678407798'][11](___1539217067(36), ___1539217067(37), ___1539217067(38), round(0+3+3+3+3)); return;}}/**/
	}

	/**
	 * Sets an option value and saves it into a DB. After saving the OnAfterSetOption event is triggered.
	 *
	 * @param string $moduleId The module ID.
	 * @param string $name The option name.
	 * @param string $value The option value.
	 * @param string $siteId The site ID, if the option depends on a site.
	 * @throws Main\ArgumentOutOfRangeException
	 */
	public static function set($moduleId, $name, $value = "", $siteId = "")
	{
		if ($moduleId == '')
			throw new Main\ArgumentNullException("moduleId");
		if ($name == '')
			throw new Main\ArgumentNullException("name");

		if ($siteId === false)
		{
			$siteId = static::getDefaultSite();
		}

		$con = Main\Application::getConnection();
		$sqlHelper = $con->getSqlHelper();

		$updateFields = [
			"VALUE" => $value,
		];

		if($siteId == "")
		{
			$insertFields = [
				"MODULE_ID" => $moduleId,
				"NAME" => $name,
				"VALUE" => $value,
			];

			$keyFields = ["MODULE_ID", "NAME"];

			$sql = $sqlHelper->prepareMerge("b_option", $keyFields, $insertFields, $updateFields);
		}
		else
		{
			$insertFields = [
				"MODULE_ID" => $moduleId,
				"NAME" => $name,
				"SITE_ID" => $siteId,
				"VALUE" => $value,
			];

			$keyFields = ["MODULE_ID", "NAME", "SITE_ID"];

			$sql = $sqlHelper->prepareMerge("b_option_site", $keyFields, $insertFields, $updateFields);
		}

		$con->queryExecute(current($sql));

		static::clearCache($moduleId);

		static::loadTriggers($moduleId);

		$event = new Main\Event(
			"main",
			"OnAfterSetOption_".$name,
			array("value" => $value)
		);
		$event->send();

		$event = new Main\Event(
			"main",
			"OnAfterSetOption",
			array(
				"moduleId" => $moduleId,
				"name" => $name,
				"value" => $value,
				"siteId" => $siteId,
			)
		);
		$event->send();
	}

	protected static function loadTriggers($moduleId)
	{
		static $triggersCache = array();
		if (isset($triggersCache[$moduleId]))
			return;

		if (preg_match("#[^a-zA-Z0-9._]#", $moduleId))
			throw new Main\ArgumentOutOfRangeException("moduleId");

		$triggersCache[$moduleId] = true;

		$path = Main\Loader::getLocal("modules/".$moduleId."/option_triggers.php");
		if ($path === false)
			return;

		include($path);
	}

	protected static function getCacheTtl()
	{
		static $cacheTtl = null;

		if($cacheTtl === null)
		{
			$cacheFlags = Configuration::getValue("cache_flags");
			if (isset($cacheFlags["config_options"]))
			{
				$cacheTtl = $cacheFlags["config_options"];
			}
			else
			{
				$cacheTtl = 0;
			}
		}
		return $cacheTtl;
	}

	/**
	 * Deletes options from a DB.
	 *
	 * @param string $moduleId The module ID.
	 * @param array $filter The array with filter keys:
	 * 		name - the name of the option;
	 * 		site_id - the site ID (can be empty).
	 * @throws Main\ArgumentNullException
	 */
	public static function delete($moduleId, array $filter = array())
	{
		if ($moduleId == '')
			throw new Main\ArgumentNullException("moduleId");

		$con = Main\Application::getConnection();
		$sqlHelper = $con->getSqlHelper();

		$deleteForSites = true;
		$sqlWhere = $sqlWhereSite = "";

		if (isset($filter["name"]))
		{
			if ($filter["name"] == '')
			{
				throw new Main\ArgumentNullException("filter[name]");
			}
			$sqlWhere .= " AND NAME = '{$sqlHelper->forSql($filter["name"])}'";
		}
		if (isset($filter["site_id"]))
		{
			if($filter["site_id"] <> "")
			{
				$sqlWhereSite = " AND SITE_ID = '{$sqlHelper->forSql($filter["site_id"], 2)}'";
			}
			else
			{
				$deleteForSites = false;
			}
		}
		if($moduleId == 'main')
		{
			$sqlWhere .= "
				AND NAME NOT LIKE '~%' 
				AND NAME NOT IN ('crc_code', 'admin_passwordh', 'server_uniq_id','PARAM_MAX_SITES', 'PARAM_MAX_USERS') 
			";
		}
		else
		{
			$sqlWhere .= " AND NAME <> '~bsm_stop_date'";
		}

		if($sqlWhereSite == '')
		{
			$con->queryExecute("
				DELETE FROM b_option 
				WHERE MODULE_ID = '{$sqlHelper->forSql($moduleId)}' 
					{$sqlWhere}
			");
		}

		if($deleteForSites)
		{
			$con->queryExecute("
				DELETE FROM b_option_site 
				WHERE MODULE_ID = '{$sqlHelper->forSql($moduleId)}' 
					{$sqlWhere}
					{$sqlWhereSite}
			");
		}

		static::clearCache($moduleId);
	}

	protected static function clearCache($moduleId)
	{
		unset(self::$options[$moduleId]);

		if (static::getCacheTtl() !== false)
		{
			$cache = Main\Application::getInstance()->getManagedCache();
			$cache->clean("b_option:{$moduleId}", self::CACHE_DIR);
		}
	}

	protected static function getDefaultSite()
	{
		static $defaultSite;

		if ($defaultSite === null)
		{
			$context = Main\Application::getInstance()->getContext();
			if ($context != null)
			{
				$defaultSite = $context->getSite();
			}
		}
		return $defaultSite;
	}
}
