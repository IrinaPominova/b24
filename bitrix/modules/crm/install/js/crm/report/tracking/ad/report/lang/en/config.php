<?php
$MESS["CRM_REPORT_TRACKING_AD_REPORT_BUILD"] = "Creating report";
$MESS["CRM_REPORT_TRACKING_AD_REPORT_TITLE_0"] = "Campaign performance";
$MESS["CRM_REPORT_TRACKING_AD_REPORT_TITLE_1"] = "Ad group performance";
$MESS["CRM_REPORT_TRACKING_AD_REPORT_TITLE_100"] = "Keyword performance";
