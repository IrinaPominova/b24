<?
$MESS["BIZPROC_MODULE_NOT_INSTALLED"] = "The Business Processes module is not installed.";
$MESS["CRM_AUTOMATION_ACCESS_DENIED"] = "Access to entity was denied.";
$MESS["CRM_AUTOMATION_NOT_AVAILABLE"] = "Automation is not available";
$MESS["CRM_AUTOMATION_NOT_AVAILABLE_SIMPLE_CRM"] = "Automation is unavailable in Simple CRM mode";
$MESS["CRM_AUTOMATION_NOT_SUPPORTED"] = "The component does not support this CRM entity.";
$MESS["CRM_MODULE_NOT_INSTALLED"] = "The CRM module is not installed.";
?>