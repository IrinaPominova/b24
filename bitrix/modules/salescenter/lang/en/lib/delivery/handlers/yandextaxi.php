<?php
$MESS["SALESCENTER_DELIVERY_HANDLERS_YANDEX_TAXI_SHORT_DESCRIPTION"] = "Use Yandex.Taxi service for delivering orders to your clients";
$MESS["SALESCENTER_DELIVERY_HANDLERS_YANDEX_TAXI_TITLE"] = "Yandex.Taxi";
$MESS["SALESCENTER_DELIVERY_HANDLERS_YANDEX_TAXI_TYPE_DESCRIPTION"] = "Taxi service";
