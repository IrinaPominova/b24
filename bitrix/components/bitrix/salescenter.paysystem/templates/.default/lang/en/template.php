<?php
$MESS["SALESCENTER_SP_ADD_PAYMENT_BUTTON"] = "Configure";
$MESS["SALESCENTER_SP_BUTTON_CLOSE"] = "Cancel";
$MESS["SALESCENTER_SP_CAHSBOX_HINT"] = "There are no cash registers. <a href='#CASHBOX_SETTINGS_LINK#'>Configure cash registers</a>";
$MESS["SALESCENTER_SP_CONNECT_HINT"] = "Use a Yandex account to authenticate";
$MESS["SALESCENTER_SP_CONNECT_PAYMENT_BUTTON"] = "Connect";
$MESS["SALESCENTER_SP_ERROR_POPUP_TITLE"] = "Error";
$MESS["SALESCENTER_SP_LINK_CONNECT"] = "How to connect a payment system";
$MESS["SALESCENTER_SP_LINK_SBP_CONNECT"] = "Learn more about rapid payment system in Bitrix24";
$MESS["SALESCENTER_SP_PARAMS_FORM_CAN_PRINT_CHECK"] = "Enable receipt printouts";
$MESS["SALESCENTER_SP_PARAMS_FORM_CASHBOX"] = "Cash registers";
$MESS["SALESCENTER_SP_PARAMS_FORM_CASHBOX_TITLE"] = "FZ-54";
$MESS["SALESCENTER_SP_PARAMS_FORM_DESCRIPTION"] = "Description";
$MESS["SALESCENTER_SP_PARAMS_FORM_IS_CASH"] = "Payment type";
$MESS["SALESCENTER_SP_PARAMS_FORM_IS_CASH_ACQUIRING"] = "Card transaction";
$MESS["SALESCENTER_SP_PARAMS_FORM_IS_CASH_CASH"] = "Cash money";
$MESS["SALESCENTER_SP_PARAMS_FORM_IS_CASH_NO_CASH"] = "Bank transfer";
$MESS["SALESCENTER_SP_PARAMS_FORM_LOGOTIP"] = "Payment system logo";
$MESS["SALESCENTER_SP_PARAMS_FORM_LOGOTIP_HINT"] = "Recommended size: 100 by 45 pixels";
$MESS["SALESCENTER_SP_PARAMS_FORM_LOGOTIP_LOAD_BUTTON"] = "Upload";
$MESS["SALESCENTER_SP_PARAMS_FORM_NAME"] = "Name";
$MESS["SALESCENTER_SP_PARAMS_FORM_NAME_HINT"] = "Example: \"Visa and MasterCard payment\". This is the name your customers will see in the payment options list";
$MESS["SALESCENTER_SP_PARAMS_FORM_TITLE"] = "Parameters";
$MESS["SALESCENTER_SP_PAYSYSTEM_BEPAID_CHECKOUT_DESCRIPTION"] = "Use the bePaid payment page for secure banking card processing in real time";
$MESS["SALESCENTER_SP_PAYSYSTEM_BEPAID_CHECKOUT_TITLE"] = "bePaid. Payment page";
$MESS["SALESCENTER_SP_PAYSYSTEM_BEPAID_WIDGET_DESCRIPTION"] = "Use the bePaid widget to accept payments without redirecting a customer to a dedicated payment page";
$MESS["SALESCENTER_SP_PAYSYSTEM_BEPAID_WIDGET_TITLE"] = "bePaid payment widget";
$MESS["SALESCENTER_SP_PAYSYSTEM_CASH_DESCRIPTION"] = "Cash payment at a pick up location or to a courier.";
$MESS["SALESCENTER_SP_PAYSYSTEM_CASH_TITLE"] = "Cash payment";
$MESS["SALESCENTER_SP_PAYSYSTEM_DELETE_CONFIRM"] = "Are you sure you want to delete this payment system?";
$MESS["SALESCENTER_SP_PAYSYSTEM_LIQPAY_DESCRIPTION"] = "LiqPay payment";
$MESS["SALESCENTER_SP_PAYSYSTEM_LIQPAY_TITLE"] = "LiqPay";
$MESS["SALESCENTER_SP_PAYSYSTEM_PAYPAL_DESCRIPTION"] = "PayPal payment";
$MESS["SALESCENTER_SP_PAYSYSTEM_PAYPAL_TITLE"] = "PayPal";
$MESS["SALESCENTER_SP_PAYSYSTEM_QIWI_DESCRIPTION"] = "Qiwi payment";
$MESS["SALESCENTER_SP_PAYSYSTEM_QIWI_TITLE"] = "Qiwi";
$MESS["SALESCENTER_SP_PAYSYSTEM_SBERBANKONLINE_DESCRIPTION"] = "Card payment processed by Sberbank.";
$MESS["SALESCENTER_SP_PAYSYSTEM_SBERBANKONLINE_TITLE"] = "Credit/debit card (Sberbank)";
$MESS["SALESCENTER_SP_PAYSYSTEM_SKB_TITLE"] = "SKB bank (payment using QR code)";
$MESS["SALESCENTER_SP_PAYSYSTEM_WEBMONEY_DESCRIPTION"] = "WebMoney payment";
$MESS["SALESCENTER_SP_PAYSYSTEM_WEBMONEY_TITLE"] = "WebMoney";
$MESS["SALESCENTER_SP_PAYSYSTEM_WOOPPAY_CHECKOUT_TITLE"] = "Woopkassa. Payment page";
$MESS["SALESCENTER_SP_PAYSYSTEM_WOOPPAY_IFRAME_TITLE"] = "Woopkassa. Payment frame";
$MESS["SALESCENTER_SP_PAYSYSTEM_YANDEXCHECKOUT_ALFABANK_DESCRIPTION"] = "Alfa click payment processed by Yandex.Checkout (version 3.x and higher)";
$MESS["SALESCENTER_SP_PAYSYSTEM_YANDEXCHECKOUT_ALFABANK_TITLE"] = "Alfa Click via Yandex.Checkout";
$MESS["SALESCENTER_SP_PAYSYSTEM_YANDEXCHECKOUT_BANK_CARD_DESCRIPTION"] = "Credit/debit card (Yandex.Checkout, version 3.x and higher)";
$MESS["SALESCENTER_SP_PAYSYSTEM_YANDEXCHECKOUT_BANK_CARD_TITLE"] = "Credit/debit card (Yandex.Checkout)";
$MESS["SALESCENTER_SP_PAYSYSTEM_YANDEXCHECKOUT_DESCRIPTION"] = "Accept payments using Smart Payment. Payment is processed using Yandex.Checkout (version 3.x and higher)";
$MESS["SALESCENTER_SP_PAYSYSTEM_YANDEXCHECKOUT_EMBEDDED_DESCRIPTION"] = "Use the Yandex.Checkout widget to accept payments using banking cards, Apple Pay, Google Pay and Sberbank Online. A ready to use payment form will be integrated with the site and added to the payment page.";
$MESS["SALESCENTER_SP_PAYSYSTEM_YANDEXCHECKOUT_EMBEDDED_TITLE"] = "Yandex.Checkout widget";
$MESS["SALESCENTER_SP_PAYSYSTEM_YANDEXCHECKOUT_QIWI_DESCRIPTION"] = "Qiwi payment processed by Yandex.Checkout (version 3.x and higher)";
$MESS["SALESCENTER_SP_PAYSYSTEM_YANDEXCHECKOUT_QIWI_TITLE"] = "Qiwi via Yandex.Checkout";
$MESS["SALESCENTER_SP_PAYSYSTEM_YANDEXCHECKOUT_SBERBANK_DESCRIPTION"] = "Sberbank Online payment processed by Yandex.Checkout (version 3.x and higher)";
$MESS["SALESCENTER_SP_PAYSYSTEM_YANDEXCHECKOUT_SBERBANK_SMS_DESCRIPTION"] = "Sberbank Online payment processed by Yandex.Checkout and confirmed by SMS (version 3.x and higher)";
$MESS["SALESCENTER_SP_PAYSYSTEM_YANDEXCHECKOUT_SBERBANK_SMS_TITLE"] = "Sberbank Online via Yandex.Checkout, verified by SMS";
$MESS["SALESCENTER_SP_PAYSYSTEM_YANDEXCHECKOUT_SBERBANK_TITLE"] = "Sberbank Online via Yandex.Checkout";
$MESS["SALESCENTER_SP_PAYSYSTEM_YANDEXCHECKOUT_TINKOFF_BANK_DESCRIPTION"] = "Aceept payments from Tinkoff Bank's clients. Payment is processed via Yandex.Checkout (version 3.x and higher)";
$MESS["SALESCENTER_SP_PAYSYSTEM_YANDEXCHECKOUT_TINKOFF_BANK_TITLE"] = "Tinkoff Bank. Payment via Yandex.Checkout";
$MESS["SALESCENTER_SP_PAYSYSTEM_YANDEXCHECKOUT_TITLE"] = "Smart payment. Payment via Yandex.Checkout";
$MESS["SALESCENTER_SP_PAYSYSTEM_YANDEXCHECKOUT_WEBMONEY_DESCRIPTION"] = "WebMoney payment processed by Yandex.Checkout (version 3.x and higher)";
$MESS["SALESCENTER_SP_PAYSYSTEM_YANDEXCHECKOUT_WEBMONEY_TITLE"] = "WebMoney via Yandex.Checkout";
$MESS["SALESCENTER_SP_PAYSYSTEM_YANDEXCHECKOUT_YANDEX_MONEY_DESCRIPTION"] = "Yandex Money payment processed by Yandex.Checkout (version 3.x and higher)";
$MESS["SALESCENTER_SP_PAYSYSTEM_YANDEXCHECKOUT_YANDEX_MONEY_TITLE"] = "Yandex.Money payment";
$MESS["SALESCENTER_SP_POPUP_BUTTON_CANCEL"] = "Cancel";
$MESS["SALESCENTER_SP_POPUP_BUTTON_CLOSE"] = "Close";
$MESS["SALESCENTER_SP_POPUP_CONTENT"] = "The changes have not been saved. Are you sure you want to close the slider panel?";
$MESS["SALESCENTER_SP_POPUP_TITLE"] = "Confirm action";
$MESS["SALESCENTER_SP_YANDEX_LOGOUT"] = "Disable";
