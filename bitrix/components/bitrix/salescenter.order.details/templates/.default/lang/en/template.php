<?
$MESS["SOD_COMMON_DISCOUNT"] = "You save";
$MESS["SOD_COMMON_SUM_NEW"] = "Items total";
$MESS["SOD_DELIVERY"] = "Delivery";
$MESS["SOD_FREE"] = "Free";
$MESS["SOD_SUB_ORDER_TITLE"] = "Order ##ACCOUNT_NUMBER# created on #DATE_ORDER_CREATE#";
$MESS["SOD_SUB_ORDER_TITLE_SHORT"] = "Order ##ACCOUNT_NUMBER#";
$MESS["SOD_SUMMARY"] = "Total";
$MESS["SOD_TAX"] = "Tax rate";
$MESS["SOD_TOTAL_WEIGHT"] = "Total weight";
$MESS["SOD_TPL_SUMOF"] = "order amount";
?>