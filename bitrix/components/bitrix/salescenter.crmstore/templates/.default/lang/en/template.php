<?
$MESS["SC_CRM_STORE_CONTAINER_CONTENT"] = "The CRM is now a fully featured online selling platform with up-to-date payment processing, receipts and delivery. Easier payment experience ensure more sales.<br>Enable the CRM Store and watch the conversion rate grow!";
$MESS["SC_CRM_STORE_CONTAINER_LINK"] = "Details";
$MESS["SC_CRM_STORE_CONTAINER_START_SELL"] = "Start selling";
$MESS["SC_CRM_STORE_CONTAINER_SUB_TITLE"] = "Watch a short video to learn<br>how Bitrix24 CRM Store will help you sell more";
$MESS["SC_CRM_STORE_CONTAINER_TITLE"] = "A new era of online sales";
$MESS["SC_CRM_STORE_TITLE"] = "CRM Store";
?>